# Import unit test class

#This Program is working for Python version 3 and higher

#Assignment Partners

# Omair Aslam (s5068592)
# Saim Zahid (s5066000)

import unittest

#dont worry about the import errors from following line, the code runs perfectly
from word_ladder import getWordsFromDictionary, inputValidation, removeUnauthorizedWords, same, build, find, findAllPaths

#Test Case to test all User Input for being empty or not
class testUserInput(unittest.TestCase):
    def testValidateInput(self):
        self.assertTrue(inputValidation, msg='User Input is Empty')

#Test Case to test file is empty or not
class testForEmptyDictionary(unittest.TestCase):
    def testValidateDictionary(self):
        self.assertTrue(getWordsFromDictionary, msg='File is Empty')

#Test case to test for not permitted words
class testUnauthorizedWords(unittest.TestCase):
    def testDictionaryKeywords(self):
        self.assertEqual(removeUnauthorizedWords(['test', 'function'],['test', 'function', 'assert', 'true']), ['assert','true'])

    def testDictionaryWithAdditionalWords(self):
        self.assertEqual(removeUnauthorizedWords(['test', 'function', 'abacus', 'medals'], ['test', 'function', 'assert', 'true']),
                         ['assert', 'true'])

#Test case to test Same letter at same indes in two words
class testMatchingWordsList(unittest.TestCase):

    def testString_0(self):
        self.assertEqual(same('list','seek'), 0)

    def testString_1(self):
        self.assertEqual(same('lead','read'), 3)



#Test case to build list of posssible words using given pattern and words in same length words list.
# This list is then used to pool words for path
class testWordsListBuilder(unittest.TestCase):
    def testWordsPatternMatch(self):
        self.assertEqual(build('lead', ['lead','load','mead'],{},[]),['lead'])

    def testSpecialWildcardWorks(self):
        self.assertEqual(build('h.de', ['hode','apple','hide'],{},[]),['hode','hide'])

    def testSeenExcludedWords(self):
        self.assertEqual(build('.ide', ['hide','apple','gide', 'mide'],{'hide': True},[]),['gide', 'mide'])



#Test Case to Find path because Find(function) returns boolean value
class testFindWordsPath(unittest.TestCase):
    def testFindWords(self):
        self.assertTrue(find)
    def testFindNoWords(self):
        self.assertNotEqual(find, True)


#Test Case to Find path because Find(function) returns boolean value
class testFindAllWordsPath(unittest.TestCase):
    def testFindAllPath(self):
        self.assertTrue(findAllPaths)
    def testFindNoWords(self):
        self.assertNotEqual(findAllPaths, True)

if __name__ == '__main__':
    unittest.main()