# Assignment Update by
# Omair Aslam (s5068592)
# Saim Zahid (s5066000)
# This Program is working for Python version 3 and higher
import re
#This function validates all user inputs to see if they are empty
def inputValidation(userPrompt):
    while True:
        userInput = input(userPrompt)
        if len(userInput) == 0:
            print("No Input given. Please try again.")
            continue
        break
    return userInput

#This function gets all the words in a form of list from the dictionary for further use
def getWordsFromDictionary(file):
    lines = file.readlines()
    for line in range(len(lines)):
        lines[line] = lines[line].rstrip()
    return lines

#This function takes input of the list of blocked words and one by one removes it from the list of
# words that we can play with
def removeUnauthorizedWords(blockedWordList, originalWordList):
  for word in blockedWordList:
    if word in originalWordList:
      originalWordList.remove(word)
  return originalWordList

#This function takes two strings and gives the number of matches of same alphabet on same indexes between
# this word and target word
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

#This function will give us a list of words from the dictionary according to the pattern given to it as a parameter
def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

#This function gives a boolean value for weather the path was found or not, also it adds the complete path from
# start word  to target word in our variable "path"
def find(word, words, seen, target, path):
  listOfWordPatterns = []
  matchingIndexes=[]
  #This if statement makes an initial array of those indexes whose characters are same for both starting and target word
  #In this way we don't need to change those indexes and we get a more shorter path
  if same(word,target) >= 1:
    for i in range(len(word)):
      if word[i] == target[i]:
        matchingIndexes.append(i)#A loop is being generated for matching indexes storage

  for i in range(len(word)):
    if i not in matchingIndexes: #This loop makes the making sure that matching indexes are not changed in order to get a more shorter path
        listOfWordPatterns += build(word[:i] + "." + word[i + 1:], words, seen, listOfWordPatterns)# With each loop a list of word are added to the list
      #according to the pattern given to it. The "." suggests it canbe any letter, so it starts from 1st position and
      # continue to go one letter ahead with each loop for example in 1st iteration our pattern will be ".ead" and
      # for second iteration it will be "L.ad" and so on

  #This if statement generates False value for this function when no path is found from source to target word.
  if len(listOfWordPatterns) == 0:
    return False

  #This line gives the number of matches between target word and the words in the list we just made above
  listOfWordPatterns = sorted([(same(w, target), w) for w in listOfWordPatterns], reverse=True)

  #The output of above list is in the form of (2, "load"). This means that when we compare load and gold
  #the number of matches are 2 between them

  # This is loop suggest that if the current word is second last before target word can be achieved or
  # the target word itself then return True
  for (match, item) in listOfWordPatterns:
    if match >= len(target) - 1: # if this word is Target word
      if match == len(target) - 1: # if this word is second last before Target word
          path.append(item)
      return True
    seen[item] = True # this is dictionary that makes all words that we have visited as seen so that we dont
    # use them again

  # This for loop iteratively add every item to path and then calls the find function again, if it returns True
  # then it prints the path after exiting the function. If function does not return True then it pops the item
  # from path and move on to check the next one.
  for (match, item) in listOfWordPatterns:
      path.append(item)
      if find(item, words, seen, target, path):
          return True
      path.pop()

# This function is simlilar to find function in way that it also gives a boolean value for weather the path was found
# or not, also it adds the complete path from start word  to target word in our variable "path". In addition
# it will also add those words in the path whose indexes are same for both words. In this way we can get all possible
# paths and donot have to consider only the shortest path
def findAllPaths(word, words, seen, target, path):
  listOfWordPatterns = []
  for i in range(len(word)):
      listOfWordPatterns += build(word[:i] + "." + word[i + 1:], words, seen, listOfWordPatterns)
  if len(listOfWordPatterns) == 0:
    return False
  listOfWordPatterns = sorted([(same(w, target), w) for w in listOfWordPatterns])
  for (match, item) in listOfWordPatterns:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in listOfWordPatterns:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()




#This while loop keeps asking for a dictionary name until user gives a valid one
while True:
    dictionaryName = inputValidation("Enter dictionary name: ")
    try:
        file = open(dictionaryName)
        break
    except:
        print("No dictionary file found with this name. Please try again.")
        continue

#This line calls the function getWordsFromDictionary. This functions returns list of all words contained in dictionary
words = getWordsFromDictionary(file)

#This while loop gets starting word from user, validates it, check if its in dictionary or not and finally makes a
# list of all the words whose length matches the length of this starting word
while True:
  startingWord = inputValidation("Enter starting word:")
  sameLengthWords = []
  if startingWord not in words:
      print("This word is not in dictionary you provided. Please Try Again.")
      continue
  else:
      for word in words:
          if len(word) == len(startingWord):
              sameLengthWords.append(word)
      break

# This while loop enables us to add those words that we dont wont in our path
while True:
  unauthorizedWordsList = []
  unauthorizedWords = inputValidation("Would you like to provide a list of words that cannot be used(Y/N)? ")
  if unauthorizedWords == "y" or unauthorizedWords == "Y":
    while True:
      blockedWord = inputValidation("Please provide a word to remove from current dictionary").lower()
      unauthorizedWordsList.append(blockedWord)
      checkForMoreBlockedWords = inputValidation("Would you like to provide more words(Y?N)?").lower()
      if checkForMoreBlockedWords == 'y':
        continue
      elif checkForMoreBlockedWords == 'n':
        break
      else:
        print("Please provide one of the following as input (Y/N) and try again")
        continue
    sameLengthWords = removeUnauthorizedWords(unauthorizedWordsList, sameLengthWords)
    break
  elif unauthorizedWords == 'n' or unauthorizedWords == 'N':
    break
  else:
    print("Please provide one of the following as input (Y/N) and try again")
    continue

#This while loops gets the target word from user and makes sure that this target word is in dictionary provided
# and also matches the length of starting word
while True:
    targetWord = inputValidation("Enter target word:")
    if targetWord not in words:
        print("This word is not in dictionary you provided. Please Try Again.")
        continue
    else:
        if len(targetWord) != len(startingWord):
            print("The word you provided must match the length of starting word which is ", len(startingWord), " Please Try Again.")
            continue
    break

#This while loop gets the input from us whether we want to chose a singlr shortest path or all possibe paths
while True:
    pathType = inputValidation("Would you like to display the shortest path or all the unique paths(single/all)? ").lower()
    if pathType == "all":
        break
    elif pathType == "single":
        break
    else:
        print("Please provide one of the following as input (single/all) and try again")
        continue



while True:
    path = [startingWord]
    visitedWords = {startingWord: True} # this is dictionary that makes all words that we have visited as seen so that we dont
    # use them again
    if pathType == 'single':
        if find(startingWord, sameLengthWords, visitedWords, targetWord, path):
            path.append(targetWord)
            print(len(path) - 1, path)
            break
        else:
            print("No path found")
            break
    elif pathType == 'all':
        for word in path:
            if word in sameLengthWords and word != startingWord and word != targetWord:
                sameLengthWords.remove(word)
            if findAllPaths(startingWord, sameLengthWords, visitedWords, targetWord, path):
                path.append(targetWord)
                print(len(path) - 1, path)
